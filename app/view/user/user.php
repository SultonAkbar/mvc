<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
   <h2>section, <?= !isset($data["company"]) ?: $data["company"] ?> </h2>
   <a href="<?= BASE_URL ?>/register" class="btn btn-primary">add user</a>
   <p>daftar user</p>
   <div class="row">
      <div class="col-lg-6">
         <?php Flasher::flash(); ?>
      </div>
   </div>
   <div class="table-responsive">
      <table class="table table-striped table-sm">
         <thead>
            <tr>
               <th scope="col">#</th>
               <th scope="col">Header</th>
               <th scope="col">Header</th>
               <th scope="col">Header</th>
               <th scope="col">Header</th>
               <th scope="col">Action</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach ($data['users'] as $data) { ?>
               <tr>
                  <td><?= $data['id'] ?></td>
                  <td><?= $data['username'] ?></td>
                  <td><?= $data['email'] ?></td>
                  <td><?= $data['first_name'] ?></td>
                  <td><?= $data['last_name'] ?></td>
                  <td><a href="<?= BASE_URL ?>/user/edit/<?= $data['id'] ?>">Edit</a></td>
               </tr>
            <?php } ?>
         </tbody>
      </table>
   </div>
</main>